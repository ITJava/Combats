package sample.game;

public class User extends Player {
    public User(String name){
        this.name = name;
        this.hitPoints = 100;
        this.strength = 25;
    }
}
