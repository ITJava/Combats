package sample.game;

import java.util.Random;

public class Mob extends Player {
    private static int count = 0;
    Random rand = new Random();

    public Mob() {
        name = "Mob #" + ++count;
        hitPoints = (100 + rand.nextInt(101));
        strength = (2 + rand.nextInt(19));
    }
     public int actionChoice(){
        return (1+ rand.nextInt(3));
     }
}
