package sample;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import sample.game.Mob;
import sample.game.User;

public class Main extends Application implements EventHandler<ActionEvent> {
    Button btn1;
    Label lb1;
    Label lb2;
    private ToggleGroup leftToggleGroup;
    private ToggleGroup rightToggleGroup;
    private RadioButton rbtn5;
    private RadioButton rbtn6;
    private RadioButton rbtn7;
    private RadioButton rbtn8;
    Mob mob = new Mob();
    User player = new User("Noob1");

    @Override
    public void start(Stage primaryStage) throws Exception {
        //Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        GridPane root = new GridPane();

        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(50);
        root.getColumnConstraints().add(0, col1);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(50);
        root.getColumnConstraints().add(1, col2);
        RowConstraints row1 = new RowConstraints();
        row1.setPercentHeight(10);
        root.getRowConstraints().add(0, row1);
        RowConstraints row2 = new RowConstraints();
        row2.setPercentHeight(50);
        root.getRowConstraints().add(1, row2);
        RowConstraints row3 = new RowConstraints();
        row3.setPercentHeight(15);
        root.getRowConstraints().add(2, row3);
        RowConstraints row4 = new RowConstraints();
        row4.setPercentHeight(25);
        root.getRowConstraints().add(3, row4);

        FlowPane pane1 = new FlowPane();
        FlowPane pane2 = new FlowPane();

        leftToggleGroup = new ToggleGroup();
        RadioButton rbtn1 = new RadioButton("Head");
        rbtn1.setUserData(1);
        RadioButton rbtn2 = new RadioButton("Torso");
        rbtn2.setUserData(2);
        RadioButton rbtn3 = new RadioButton("Bottom");
        rbtn3.setUserData(3);
        RadioButton rbtn4 = new RadioButton("Legs");
        rbtn4.setUserData(4);
        rbtn1.setToggleGroup(leftToggleGroup);
        rbtn2.setToggleGroup(leftToggleGroup);
        rbtn3.setToggleGroup(leftToggleGroup);
        rbtn4.setToggleGroup(leftToggleGroup);
        leftToggleGroup.selectToggle(rbtn1);
        VBox vb = new VBox(10, rbtn1, rbtn2, rbtn3, rbtn4);
        pane1.getChildren().add(vb);

        rightToggleGroup = new ToggleGroup();
        rbtn5 = new RadioButton("Head and Torso");
        rbtn5.setUserData(1);
        rbtn6 = new RadioButton("Torso and Bottom");
        rbtn6.setUserData(2);
        rbtn7 = new RadioButton("Bottom and Legs");
        rbtn7.setUserData(3);
        rbtn8 = new RadioButton("Legs and Head");
        rbtn8.setUserData(4);
        rbtn5.setToggleGroup(rightToggleGroup);
        rbtn6.setToggleGroup(rightToggleGroup);
        rbtn7.setToggleGroup(rightToggleGroup);
        rbtn8.setToggleGroup(rightToggleGroup);
        VBox vb2 = new VBox(10, rbtn5, rbtn6, rbtn7, rbtn8);
        pane2.getChildren().add(vb2);

        TextArea textArea = new TextArea();
        textArea.setText("Lorem ipsum dolor sit amet, consectetur " +
                "adipiscing elit, sed do eiusmod tempor incididunt " +
                "ut labore et dolore magna aliqua. Ut enim ad minim " +
                "veniam, quis nostrud exercitation ullamco laboris " +
                "nisi ut aliquip ex ea commodo consequat. Duis aute " +
                "irure dolor in reprehenderit in voluptate velit esse " +
                "cillum dolore eu fugiat nulla pariatur. " +
                "Excepteur sint occaecat cupidatat non proident, " +
                "sunt in culpa qui officia deserunt " +
                "mollit anim id est laborum.");
        textArea.setWrapText(true);
        textArea.setEditable(false);

        lb1 = new Label("You");
        lb2 = new Label("Foe");
        btn1 = new Button("hit");

        root.add(lb1, 0, 0);
        root.add(lb2, 1, 0);
        root.add(pane1, 0, 1);
        root.add(pane2, 1, 1);
        root.add(btn1, 0, 2);
        root.add(textArea, 0, 3);

        GridPane.setColumnSpan(btn1, 2);
        GridPane.setColumnSpan(textArea, 2);

        GridPane.setHalignment(btn1, HPos.CENTER);
        GridPane.setValignment(btn1, VPos.CENTER);
        vb.setAlignment(Pos.CENTER_LEFT);
        pane1.setAlignment(Pos.CENTER);
        //pane1.setStyle("-fx-background-color: #00ff00");
        vb2.setAlignment(Pos.CENTER_LEFT);
        pane2.setAlignment(Pos.CENTER);
        GridPane.setValignment(lb1, VPos.CENTER);
        GridPane.setHalignment(lb1, HPos.CENTER);
        GridPane.setValignment(lb2, VPos.CENTER);
        GridPane.setHalignment(lb2, HPos.CENTER);
        rightToggleGroup.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> observable, Toggle oldValue, Toggle newValue) {
                onToggleChange(newValue);

            }
        });
        btn1.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                nextIteration();
            }
        });
        root.setAlignment(Pos.CENTER);
        root.setGridLinesVisible(true);
        //primaryStage.setFullScreen(true);
        rightToggleGroup.selectToggle(rbtn5);

        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

    private void onToggleChange(Toggle toggle) {
        int selectedToggleId = (Integer) toggle.getUserData();
        rbtn5.setStyle("-fx-background-color: #F4F4F4;");
        rbtn6.setStyle("-fx-background-color: #F4F4F4;");
        rbtn7.setStyle("-fx-background-color: #F4F4F4;");
        rbtn8.setStyle("-fx-background-color: #F4F4F4;");
        switch (selectedToggleId) {
            case 1:
                rbtn5.setStyle("-fx-background-color: #D0D0D0;");
                rbtn6.setStyle("-fx-background-color: #D0D0D0");
                break;
            case 2:
                rbtn6.setStyle("-fx-background-color: #D0D0D0");
                rbtn7.setStyle("-fx-background-color: #D0D0D0");
                break;
            case 3:
                rbtn7.setStyle("-fx-background-color: #D0D0D0;");
                rbtn8.setStyle("-fx-background-color: #D0D0D0;");
                break;
            case 4:
                rbtn8.setStyle("-fx-background-color: #D0D0D0;");
                rbtn5.setStyle("-fx-background-color: #D0D0D0;");
                break;
        }


    }

    private void nextIteration() {
        int selectedLeft = (Integer) (leftToggleGroup.getSelectedToggle().getUserData());
        int selectedRight = (Integer) (rightToggleGroup.getSelectedToggle().getUserData());
        System.out.println("Left - " + selectedLeft + ", Right - " + selectedRight);
        while (mob.getHitPoints() > 0 && player.getHitPoints() > 0) {
            int mobDefenceAction = mob.actionChoice();
            int mobAttackAction = mob.actionChoice();
            int playerDefenceAction = (int) rightToggleGroup.getSelectedToggle().getUserData();
            int playerAttackAction = (int) leftToggleGroup.getSelectedToggle().getUserData();
            if(mobAttackAction != playerDefenceAction && mobAttackAction != playerDefenceAction+1){
                if(playerDefenceAction == 4 && mobAttackAction == 1){
                    //TODO mob missed
                } else {
                    player.setHitPoints(player.getHitPoints()-mob.getStrength());
                }
            }
            if(playerAttackAction != mobDefenceAction && playerAttackAction != mobDefenceAction+1){
                if(mobDefenceAction == 4 && playerAttackAction == 1){
                    //TODO players missed
                } else {
                    mob.setHitPoints(mob.getHitPoints()-player.getStrength());
                }
            }
            System.out.println("Player HP = " + player.getHitPoints() + " - Mob HP = " + mob.getHitPoints());
        }
        if (mob.getHitPoints()<=0 && player.getHitPoints()<=0){
            System.out.println("draw");
        } else if (mob.getHitPoints()<=0){
            System.out.println("You win!");
        } else {
            System.out.println("You died!");
        }
        System.exit(0);
    }

    @Override
    public void handle(ActionEvent event) {
        System.out.println(leftToggleGroup.getSelectedToggle().toString());
        System.out.println(leftToggleGroup.getSelectedToggle().getUserData());
        System.out.println("Button clicked " + event.getSource());
    }
}
